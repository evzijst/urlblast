#!/usr/bin/env python

from interruptingcow import timeout
import getopt
from requests import sessions
import sys
import time

class TimeoutException(Exception):
    pass

options, remainder = getopt.getopt(sys.argv[1:], 't:k:',
                                   ['time=', 'keepalive='])
duration = 1
keepalive = True

def die(message=None):
    if message:
        print >> sys.stderr, message
    else:
        print >> sys.stderr, """Request a URL.

Usage: %s OPTIONS url

OPTIONS
    -t, --time          length of the test (seconds, float)
    -k, --keepalive     toggle keepalive [yes|no] (default is yes)
""" % sys.argv[0]
    exit(1)

for opt, arg in options:
    if opt in ('-t', '--time'):
        try:
            duration = float(arg)
            if duration < 0.0:
                raise ValueError
        except ValueError:
            die('Error: invalid duration: %s' % arg)
    elif opt in ('-k', '--keepalive'):
        if arg.lower() in ('no', 'n', '0', 'off'):
            keepalive = False
    else:
        die()
if len(remainder) is not 1:
    die()
url = remainder[0]

start = time.time()
count = 0
try:
    with sessions.session() as conn:
        with timeout(duration, TimeoutException):
            while True:
                result = conn.get(url, verify=False, headers={'Connection':'close'} if not keepalive else None)
                count += 1
except TimeoutException:
    end = time.time()
    print 'Made %d requests in %.3f seconds.' % (count, end - start)
    print 'Rate: %.3f requests/sec' % (count / (end - start))

